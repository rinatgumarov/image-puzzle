import React, {FC} from 'react'
import {BrowserRouter, Route, Switch} from 'react-router-dom'
import FirstImage from './FirstImage'
import SecondImage from './SecondImage'

export const App: FC = () => {
  return (
    <BrowserRouter>
      <Switch>
        <Route path={'/'} exact component={FirstImage} />
        <Route
          path={'/yaay-3000-ogogo-happy-g87'}
          exact
          component={SecondImage}
        />
      </Switch>
    </BrowserRouter>
  )
}
